<!DOCTYPE>
<!
Author: Kristine Gayle Nebrija
>
<?php
include("functions/functions.php");
?>
<html>
	<head>
		<title> Gayle and Co. </title>

		<link href="css/bootstrap.min.css" rel="stylesheet">
  	  	<link href="css/shop-homepage.css" rel="stylesheet">
  	  	<link href="styles/style.css" rel="stylesheet" media="all">
        <script src="//tinymce.cachefly.net/4.2/tinymce.min.js"></script>
        <script>tinymce.init({selector:'textarea'});</script>
     
	</head>

<body style="background-color:80cfc9">
	<div class="container"> 

        <div  style="padding-top:50px">
            <center>
                <img src="images/header.png" alt="">
            </center>
        </div>

        <div class="row"  style="padding-top:50px">
           
            <div class="col-md-3">
                <div class="list-group">
                    <a href="index.php" class="list-group-item" style="background-color:80cfc9">Home</a>
                    <a href="#collapseProducts" class="list-group-item" data-toggle="collapse" style="background-color:80cfc9">Products</a>

                      <div class="panel-collapse collapse" id="collapseProducts" style="background-color:80cfc9">
                        <div class="panel-body">
                            <a href='allproducts.php' class='list-group-item' style='background-color:80cfc9'>All Products</a>
                           <?php
                            getTypes();
                           ?>


                        </div>
                      </div>
                    <a href="cart.php" class="list-group-item" style="background-color:80cfc9"> Shopping Cart (<b> <?php  total_items(); ?> </b>) </a>
                    <a href="contact.php" class="list-group-item" style="background-color:80cfc9">Contact Us</a>
                </div>
            </div>



        

        <div class="col-md-9" align="right">
             <div class="row carousel-holder">
                <div class="col-md-12">
                    <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
                        <ol class="carousel-indicators">
                            <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
                            <li data-target="#carousel-example-generic" data-slide-to="1"></li>
                            <li data-target="#carousel-example-generic" data-slide-to="2"></li>
                        </ol>
                        <div class="carousel-inner">
                            <div class="item active">
                                <img class="slide-image" src="images/scroll1.jpg" alt="">
                            </div>
                            <div class="item">
                                <img class="slide-image" src="images/scroll2.jpg" alt="">
                            </div>
                            <div class="item">
                                <img class="slide-image" src="images/scroll3.jpg" alt="">
                            </div>
                        </div>
                        <a class="left carousel-control" href="#carousel-example-generic" data-slide="prev">
                            <span class="glyphicon glyphicon-chevron-left"></span>
                        </a>
                        <a class="right carousel-control" href="#carousel-example-generic" data-slide="next">
                            <span class="glyphicon glyphicon-chevron-right"></span>
                        </a>
                    </div>
                </div>
            </div>

                <div>
                     <form action = ' ' method ='post' enctype='multipart/form-data'>
                        <table class='table' >
                           <tr align = "center"  height="50"  >
                              <td colspan="5"><h2  style="font-size:25px"> Shopping Cart </h2></td>
                            </tr>
                            <tr>
                            </tr>
                            <tr height = "30" align="center" > 

                                <td> Remove </td>
                                <td> Product/s </td>
                                <td> Quantity </td>
                                <td> Total Price </td>
                                 <?php
                                     cart();
                                     showCart();
                                     click();
                                     
                                 ?>   
                            </tr>
                    
                            <tr align='center'>
                                <td> <input class='button' type ='submit' name = 'remove_cart' value = 'Remove' style='text-decoration:none; color:black';/> </td>
                                <td>  <input class='button' type ='submit' name = 'continue' value = 'Continue Shopping' style='text-decoration:none; color:black';/> </td>
                                <td> <input class='button' type ='submit' name = 'update_cart' value = 'Update Quantity' style='text-decoration:none; color:black';/></td>
                                <td> <b> SubTotal: <?php total_price(); ?> Php </b></td>
                            </tr>
           
                            <tr align='center' > <td colspan="10"><button class='button'> 
                            <a href = 'checkout.php' style='text-decoration:none; color:black;'>Checkout</a></button></td>
                            </tr>

                        </table>
                    </form>
                </div>
            </div>

        
        </div>

        </div>
         <div style="padding:50px">
            <h3 style="font-size:20px">
            <center>
                <img src="images/footer.png" alt=""> 
            </center>
            </h3>
        </div>
    </div>


      <!-- jQuery -->
    <script src="js/jquery.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="js/bootstrap.min.js"></script>


</body>
</html>