<!DOCTYPE>
<?php
	include("includes/db.php");
?>
<html>
	<head>
		<title>Inserting Products</title>
		<script src="//tinymce.cachefly.net/4.2/tinymce.min.js"></script>
		<script>tinymce.init({selector:'textarea'});</script>
		<link href="css/bootstrap.min.css" rel="stylesheet">
  	  	<link href="css/shop-homepage.css" rel="stylesheet">


<body>
	<form action = "insert_product.php" method ="post" enctype="multipart/form-data">
		<table align="center" border=2px>
			<tr align = "center">
				<td colspan="8"><h2>Insert New Post Here</h2></td>
			</tr>

			<tr>
				<td><b>Jewelery Name:</b> </td>
				<td><input type = "text" name = "product_name" size="100" required> </td>
			</tr>

			<tr>
				<td><b>Description:</b> </td>
				<td><textarea name="product_desc" cols="20" rows="10" ></textarea></td>
			</tr> 

			<tr>
				<td><b>Type:</b> </td>
				<td>
				<select name="product_type">
					<option>Select a Type</option>
					<?php
						
						$get_type = "select * from type";
						$run_type = mysqli_query($con,$get_type);

						while ($row_type=mysqli_fetch_array($run_type)){
							$type_id = $row_type['type_id'];
							$type_title = $row_type['type_title'];
						
							echo "<option value='$type_id'>$type_title</option>";
						}
						
					?>
				</select>
				</td>
			</tr>

			<tr>
				<td><b>Image:</b> </td>
				<td><input type = "file" name = "product_image" required> </td>
			</tr>


			<tr>
				<td><b>Price:</b> </td>
				<td><input type = "text" name = "product_price" required> </td>
			</tr>

		

			<tr align="center">
				<td colspan="8"><input type = "submit" name = "insert_post" value="Insert Now"> </td>
			</tr>

		</table>

	</form>

	
</body>
</html>

<?php
	if(isset($_POST['insert_post'])){
		$product_name = $_POST['product_name'];
		$product_desc = $_POST['product_desc'];
		$product_type = $_POST['product_type'];
		$product_price = $_POST['product_price'];

		$product_image = $_FILES['product_image']['name'];
		$product_image_tmp = $_FILES['product_image']['tmp_name'];

		move_uploaded_file($product_image_tmp, "product_images/$product_image");
		
		$insert_product = "insert into product (product_name, product_desc, product_type, product_price, product_image) values ('$product_name', '$product_desc', '$product_type', '$product_price', '$product_image')";
		$insert_pro = mysqli_query($con,$insert_product);

		if($insert_pro){
			echo "<script>alert('Product has been inserted') </script>";
			echo "<script>window.open('insert_product.php','_self')</script>";
		}
	}
?>