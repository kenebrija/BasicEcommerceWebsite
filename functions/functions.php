<?php

include("db.php");

///getting the types

function getTypes(){
	global $con;
	$get_type = "select * from type";
	$run_type = mysqli_query($con,$get_type);

	while ($row_type=mysqli_fetch_array($run_type)){
		$type_id = $row_type['type_id'];
		$type_title = $row_type['type_title'];
	
		echo " <a href='index.php?type=$type_id' class='list-group-item' style='background-color:80cfc9'>$type_title</a>";
	}
}

function deleteAll(){
	global $con;
	
	$delete_all = "DELETE FROM CART";
	$run_delete = mysqli_query($con, $delete_all);

}
function getProdType(){

	if(isset($_GET['type'])){

		$type_id = $_GET['type'];

		global $con;
		$get_type_pro = "select * from product where product_type='$type_id'";
		$run_type_pro = mysqli_query($con, $get_type_pro);
		$y=0;
		while($row_type_pro = mysqli_fetch_array($run_type_pro)){
			$pro_id = $row_type_pro['product_id'];
			$pro_name = $row_type_pro['product_name'];
			$pro_desc = $row_type_pro['product_desc'];
			$pro_type =$row_type_pro['product_type'];
			$pro_price = $row_type_pro['product_price'];
			$pro_image = $row_type_pro['product_image'];
			$y++;
			echo"
				 <div class= 'col-xs-4'>
					<center>
						<h2> <br> </h2>
						<h3 style = 'font-size:15px'><b>$pro_name</b></h3>
						<img src = 'admin_area/product_images/$pro_image' width ='210', height='210', style='border:2px solid black'>
						<p> Price: $pro_price Php </p>
						<a href = 'details.php?pro_id=$pro_id'><button style='float:left' class='button'> Details </button> </a>
						<a href = 'index.php?add_cart=$pro_id'><button style='float:right' class='button'> Add to Cart </button> </a>
						<br>
					
				</center>
				</div>
					

			";
		}
	}
}

function getAllProd(){

	


	global $con;
	$get_type_pro = "select * from product";
	$run_type_pro = mysqli_query($con, $get_type_pro);
	$y=0;
	while($row_type_pro = mysqli_fetch_array($run_type_pro)){
		$pro_id = $row_type_pro['product_id'];
		$pro_name = $row_type_pro['product_name'];
		$pro_desc = $row_type_pro['product_desc'];
		$pro_type =$row_type_pro['product_type'];
		$pro_price = $row_type_pro['product_price'];
		$pro_image = $row_type_pro['product_image'];
		$y++;
		echo"
			 <div class= 'col-xs-4'>
				<center>
					<h2> <br> </h2>
					<h3 style = 'font-size:15px'><b>$pro_name</b></h3>
					<img src = 'admin_area/product_images/$pro_image' width ='210', height='210', style='border:2px solid black'>
					<p> Price: $pro_price Php </p>
					<a href = 'details.php?pro_id=$pro_id'><button style='float:left' class='button'> Details </button> </a>
					<a href = 'index.php?add_cart=$pro_id'><button style='float:right' class='button'> Add to Cart </button> </a>
					<br>
				
			</center>
			</div>
				

		";
	}
	
	
}


function cart(){
	if(isset($_GET['add_cart'])){
		global $con;
		$ip = getIp();
		$pro_id = $_GET['add_cart'];
		$check_pro = "select * from cart where ip_add='$ip AND p_id='$pro_id'";
		$run_check = mysqli_query($con, $check_pro);

		if(mysqli_num_rows($run_check)>0){
			echo "";
		}else{
			$insert_pro = "insert into cart (p_id,ip_add) values ('$pro_id', '$ip')";
			$run_pro = mysqli_query($con, $insert_pro);
			echo "<script> window.open('index.php','_self')</script>";
		}
	}	
}

function  total_items(){
	if(isset($_GET['add_cart'])){
		global $con;
		$ip = getIp();
		$get_items = "select * from cart where ip_add='$ip'";
		$run_items = mysqli_query($con, $get_items);
		$count_items = mysqli_num_rows($run_items);
	}else{
		global $con;
		$ip = getIp();
		$get_items = "select * from cart where ip_add='$ip'";
		$run_items = mysqli_query($con, $get_items);
		$count_items = mysqli_num_rows($run_items);
	}
	echo $count_items;
}

function total_price(){
	global $con;
	$total = 0;
	$ip = getIp();
	$sel_price = "select * from cart where ip_add='$ip'";
	$run_price = mysqli_query($con, $sel_price);

	while($p_price=mysqli_fetch_array($run_price)){
		$pro_id = $p_price['p_id'];
		$pro_price = "select * from product where product_id='$pro_id'";
		$run_pro_price = mysqli_query($con, $pro_price);

		while($pp_price = mysqli_fetch_array($run_pro_price)){
			$product_price = array($pp_price['product_price']);
			$values = array_sum($product_price);
			$qty = $p_price['qty'];
			$values = $values * $qty;

			$total += $values;
		}
	}
	echo $total;

}

function getIp() {
    $ip = $_SERVER['REMOTE_ADDR'];
 
    if (!empty($_SERVER['HTTP_CLIENT_IP'])) {
        $ip = $_SERVER['HTTP_CLIENT_IP'];
    } elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
        $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
    }
 
    return $ip;
}
 
function getPro(){

	if(!isset($_GET['type'])){
		global $con;
		$get_pro = "select * from product order by RAND() LIMIT 0,3";
		$run_pro = mysqli_query($con, $get_pro);

		while($row_pro = mysqli_fetch_array($run_pro)){
			$pro_id = $row_pro['product_id'];
			$pro_name = $row_pro['product_name'];
			$pro_desc = $row_pro['product_desc'];
			$pro_type = $row_pro['product_type'];
			$pro_price = $row_pro['product_price'];
			$pro_image = $row_pro['product_image'];

			echo "
				 <div class= 'col-xs-4'>
					<center>
						<h3 style = 'font-size:15px'><b>$pro_name</b></h3>
						<img src = 'admin_area/product_images/$pro_image' width ='210', height='210', style='border:2px solid black'>
						<p> Price: $pro_price Php </p>
						<a href = 'details.php?pro_id=$pro_id'><button style='float:left' class='button'> Details </button> </a>
						<a href = 'index.php?add_cart=$pro_id'><button style='float:right' class='button'> Add to Cart </button> </a>
						<br>
				</center>
				</div>
					

			";
		}
	}
	
}

function showCart(){

	global $con;
	$total = 0;
	$ip = getIp();
	$sel_price = "select * from cart where ip_add='$ip'";
	$run_price = mysqli_query($con, $sel_price);


	while($p_price=mysqli_fetch_array($run_price)){
		$pro_id = $p_price['p_id'];
		$pro_price = "select * from product where product_id='$pro_id'";
		$row_cart = mysqli_query($con, $pro_price);
		$qty = $p_price['qty'];
		while($pp_price = mysqli_fetch_array($row_cart)){
			$pro_name = $pp_price['product_name'];
			$pro_price = $pp_price['product_price'] ;
			$pro_image = $pp_price['product_image'];

			echo "
				
				<tr>
				<td align='center' > <input type = 'checkbox' name='remove[]' value='$pro_id'/></td>
				<td align='center'> $pro_name <br>
				<img src = 'admin_area/product_images/$pro_image' width='100' height='100' style='border:2px solid black'>
				<td align = 'center'> <input type='text' size='1' name='qty[]' value='$qty'/></td>
				<td align = 'center'> $pro_price Php  </td>
				</tr>

			";
			

		}
		
	}
}
function click(){
	global $con;
    $ip = getIp();

    $x=0;

    if(isset($_POST['remove_cart'])){
        foreach($_POST['remove'] as $remove_id){
         	 $delete_product = "delete from cart where p_id='$remove_id' AND ip_add='$ip'";
             $run_delete = mysqli_query($con, $delete_product);
        	print_r("$remove_id");

            if($run_delete){
                echo "<script> window.open('cart.php','_self')</script>";
            }

        }
    }

    if(isset($_POST['continue'])){
        echo "<script> window.open('index.php','_self') </script>";
    }

    if(isset($_POST['update_cart'])){
		$xx=0;
		$type_id = $_POST['update_cart'];

		global $con;
		$get_type_pro = "select p_id from cart where ip_add='$ip'";
		$run_type_pro = mysqli_query($con, $get_type_pro);
		$arr = array();

		$index=0;
		while($row = mysqli_fetch_assoc($run_type_pro)) // loop to give you the data in an associative array so you can use it however.
		{
		     $arr[$index] = $row;
		     $index++;
		}	

		foreach($_POST['qty'] as $qty){
			$id_qty = $arr[$xx]['p_id'];
			$insert_product = "update cart set qty=$qty where p_id=$id_qty AND ip_add='$ip' ";
			$run_insert = mysqli_query($con, $insert_product);
			//echo " $qty,$id_qty ";
			$xx++;
		}

		echo "<script> window.open('cart.php','_self') </script>";
	}


}


function subtotal(){

}
?>
