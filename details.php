<!DOCTYPE>
<!
Author: Kristine Gayle Nebrija
>
<?php
include("functions/functions.php");
?>
<html>
	<head>
		<title> Gayle and Co. </title>

		<link href="css/bootstrap.min.css" rel="stylesheet">
  	  	<link href="css/shop-homepage.css" rel="stylesheet">
  	  	<link href="styles/style.css" rel="stylesheet" media="all">
	</head>

<body style="background-color:80cfc9">
	<div class="container"> 

		<div  style="padding-top:50px">
			<center>
				<img src="images/header.png" alt="">
			</center>
		</div>

		<div class="row"  style="padding-top:50px">
           
            <div class="col-md-3">
                <div class="list-group">
                    <a href="index.php" class="list-group-item" style="background-color:80cfc9">Home</a>
                    <a href="#collapseProducts" class="list-group-item" data-toggle="collapse" style="background-color:80cfc9">Products</a>

                      <div class="panel-collapse collapse" id="collapseProducts" style="background-color:80cfc9">
                        <div class="panel-body">
                           <?php
                            getTypes();
                           ?>


                        </div>
                      </div>
                    <a href="cart.php" class="list-group-item" style="background-color:80cfc9">Shopping Cart</a>
                    <a href="contact.php" class="list-group-item" style="background-color:80cfc9">Contact Us</a>
                </div>
            </div>



        

        <div class="col-md-9">

            <div class="row carousel-holder">
                <div class="col-md-12">
                    <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
                        <ol class="carousel-indicators">
                            <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
                            <li data-target="#carousel-example-generic" data-slide-to="1"></li>
                            <li data-target="#carousel-example-generic" data-slide-to="2"></li>
                        </ol>
                        <div class="carousel-inner">
                            <div class="item active">
                                <img class="slide-image" src="images/scroll1.jpg" alt="">
                            </div>
                            <div class="item">
                                <img class="slide-image" src="images/scroll2.jpg" alt="">
                            </div>
                            <div class="item">
                                <img class="slide-image" src="images/scroll3.jpg" alt="">
                            </div>
                        </div>
                        <a class="left carousel-control" href="#carousel-example-generic" data-slide="prev">
                            <span class="glyphicon glyphicon-chevron-left"></span>
                        </a>
                        <a class="right carousel-control" href="#carousel-example-generic" data-slide="next">
                            <span class="glyphicon glyphicon-chevron-right"></span>
                        </a>
                    </div>
                </div>
            </div>


               
               <?php

                if(isset($_GET['pro_id'])){

                    $product_id = $_GET['pro_id'];

                    $get_pro = "select * from product where product_id='$product_id'";
                    $run_pro = mysqli_query($con, $get_pro);

                    while($row_pro = mysqli_fetch_array($run_pro)){
                        $pro_id = $row_pro['product_id'];
                        $pro_name = $row_pro['product_name'];
                        $pro_desc = $row_pro['product_desc'];
                        $pro_price = $row_pro['product_price'];
                        $pro_image = $row_pro['product_image'];
                       

                        echo "
                    
                            
                                    <center>
                                    <img src = 'admin_area/product_images/$pro_image' width ='400', height='300'>
                                    <table class='table'>
                                        <tr>
                                        <td style='width:85px'> Name of Product: </td>
                                        <td align='left'> $pro_name </td>
                                        </tr>

                                        <tr>
                                        <td  style='width:85px'> Description: </td>
                                        <td> $pro_desc  </td>
                                        </tr>

                                        <tr>
                                        <td  style='width:90px'> Price: </td>
                                        <td> $pro_price  Php </td>
                                        </tr>

                                    </table>
                                   
                                
                            
                                    
                                    <a href = 'index.php' ><button style='float:left'class='button'> Go Back </button> </a>
                                    <a href = 'index.php?add_cart=$pro_id'><button style='float:right' class='button'> Add to Cart </button> </a>
                                
                            


                        ";
                    }
                }
               ?>
          
        </div>

        </div>
         <div style="padding:50px">
            <h3 style="font-size:20px">
            <center>
                 <img src="images/footer.png" alt="">
            </center>
            </h3>
        </div>
    </div>


      <!-- jQuery -->
    <script src="js/jquery.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="js/bootstrap.min.js"></script>

</body>
</html>