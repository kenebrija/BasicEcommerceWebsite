<!DOCTYPE>
<!
Author: Kristine Gayle Nebrija
>
<?php
include("functions/functions.php");
?>
<html>
    <head>
        <title> Gayle and Co. </title>

        <link href="css/bootstrap.min.css" rel="stylesheet">
        <link href="css/shop-homepage.css" rel="stylesheet">
        <link href="styles/style.css" rel="stylesheet" media="all">
    </head>

<body style="background-color:80cfc9">

    <div class="container"> 
        
        <div  style="padding-top:50px">
            <center>
                <img src="images/header.png" alt="">
            </center>
        </div>

        <div class="row"  style="padding-top:50px">
           
            <div class="col-md-3">
                <div class="list-group">
                    <a href="index.php" class="list-group-item" style="background-color:80cfc9">Home</a>
                    <a href="#collapseProducts" class="list-group-item" data-toggle="collapse" style="background-color:80cfc9">Products</a>

                      <div class="panel-collapse collapse" id="collapseProducts" style="background-color:80cfc9">
                        <div class="panel-body">
                             <a href='allproducts.php' class='list-group-item' style='background-color:80cfc9'>All Products</a>
                           <?php
                            getTypes();
                           ?>

                        </div>
                      </div>
                    <a href="cart.php" class="list-group-item" style="background-color:80cfc9"> Shopping Cart (<b> <?php  total_items(); ?> </b>) </a>
                    <a href="contact.php" class="list-group-item" style="background-color:80cfc9">Contact Us</a>
                </div>
            </div>

        <div class="col-md-9">
          
            <div class="col-md-12">
           
             <img src="images/contactus.png" alt="">;
        
            </div>
    
        </div>

        </div>
        
    </div>

    <?php cart(); ?>

      <!-- jQuery -->
    <script src="js/jquery.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="js/bootstrap.min.js"></script>

</body>
</html>