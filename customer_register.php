<!DOCTYPE>
<!
Author: Kristine Gayle Nebrija
>

<?php
session_start();
include("functions/functions.php");
include("db.php");

?>
<html>
    <head>
        <title> Gayle and Co. </title>

        <link href="css/bootstrap.min.css" rel="stylesheet">
        <link href="css/shop-homepage.css" rel="stylesheet">
        <link href="styles/style.css" rel="stylesheet" media="all">
        <link href="styles/style1.css" rel="stylesheet" media="all">
    </head>

<body style="background-color:80cfc9">

    <div class="container"> 
        
        <div  style="padding-top:50px">
            <center>
                <img src="images/header.png" alt="">
            </center>
        </div>

        <div class="row"  style="padding-top:50px">
           
           <div class="col-md-4">
            </div>
            <div class="col-md-4" style="width:100% height:100%">
                <form action="customer_register.php" method="post" enctype="mulitpart/form-data">
                    <table class="table table-bordered" align = "center" width="600" border=2px>
                        <tr align="center">
                            <td colspan="6"><h2> Create an account </h2></td>
                        </tr>
                        <tr>
                            <td align="right"> Name: </td>
                            <td><input type="text" name="c_name"/> </td>
                        </tr>
                        
                        <tr>
                            <td align="right">Email Address: </td>
                            <td> <input type="text" name="c_email"/></td>
                        </tr>
                        
                        <tr>
                            <td align="right">Password: </td>
                            <td> <input type="password" name="c_pass"/></td>
                        </tr>

                         <tr>
                            <td align="right">Address: </td>
                            <td> <input type="text" name="c_add"/></td>
                        </tr>

                        <tr>
                            <td align="right">Mobile Number: </td>
                            <td><input type="text" name="c_num"/> </td>
                        </tr>

                        <tr align="center">
                            <td colspan="6"><input type="submit" name="register" value="Create account"/> </td>
                        </tr>

                    </table>
                </form>
            </div>

        <?php
            global $con; 

            if(isset($_POST['register'])){
                $ip = getIp();
                $c_name = $_POST['c_name'];
                $c_email = $_POST['c_email'];
                $c_pass = $_POST['c_pass'];
                $c_add = $_POST['c_add'];
                $c_num = $_POST['c_num'];

                $insert_c = "INSERT INTO CUSTOMER (customer_ip, customer_name, customer_email, customer_pass, customer_address, customer_mobilenum) VALUES ('$ip', '$c_name', '$c_email', '$c_pass', '$c_add', '$c_num')";
                $run_c = mysqli_query($con, $insert_c);

                $sel_cart = "SELECT * FROM CART WHERE ip_add='$ip'";
                $run_cart = mysqli_query($con, $sel_cart);

                $check_cart = mysqli_num_rows($run_cart);


                if($check_cart==0){
                    $_SESSION['customer_email'] = $c_email;
                    echo "<script> alert('Registration successful!') </script>";
                    echo "<script> window.open('index.php','_self')</script>";
                }else{
                    $_SESSION['customer_email'] = $c_email;
                    echo "<script> alert('Registration successful! You will be redirected to checkout') </script>";
                    echo "<script> window.open('checkout.php','_self')</script>";

                }
            }

        ?>

    
        </div>

        <div style="padding:50px">

            <h3 style="font-size:20px">
            <center>
                <img src="images/footer.png" alt="">  
            </center>
            </h3>
        </div>
    </div>

      <!-- jQuery -->
    <script src="js/jquery.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="js/bootstrap.min.js"></script>

</body>
</html>