<!DOCTYPE>
<!
Author: Kristine Gayle Nebrija
>
<?php
session_start();
include("functions/functions.php");
?>
<html>
	<head>
		<title> Gayle and Co. </title>

		<link href="css/bootstrap.min.css" rel="stylesheet">
  	  	<link href="css/shop-homepage.css" rel="stylesheet">
  	  	<link href="styles/style.css" rel="stylesheet" media="all">
        
	</head>

<body style="background-color:80cfc9">

	<div class="container"> 
        
		<div  style="padding-top:50px">
			<center>
				<img src="images/header.png" alt="">
			</center>
		</div>

		<div class="row"  style="padding-top:50px">
           
            <div class="col-md-3">
                <div class="list-group">
                    <a href="index.php" class="list-group-item" style="background-color:80cfc9">Home</a>
                    <a href="#collapseProducts" class="list-group-item" data-toggle="collapse" style="background-color:80cfc9">Products</a>

                      <div class="panel-collapse collapse" id="collapseProducts" style="background-color:80cfc9">
                        <div class="panel-body" style="background-color:80cfc9">
                            <a href='allproducts.php' class='list-group-item' style='background-color:80cfc9'>All Products</a>
                           <?php
                            getTypes();
                           ?>
                        </div>
                      </div>
                    <a href="cart.php" class="list-group-item" style="background-color:80cfc9"> Shopping Cart (<b> <?php  total_items(); ?> </b>) </a>
                    <a href="contact.php" class="list-group-item" style="background-color:80cfc9">Contact Us</a>
                </div>
            </div>



        

        <div class="col-md-12">

            <div>
            <table style="width:100%;" >
                <tr>
                  <td> 
                    <?php
                        if(isset($_SESSION['customer_email'])){
                            echo "Welcome " . $_SESSION['customer_email'] . "!";
                        }else{
                            echo "Welcome Guest!";
                        }
                    ?>
                  </td>
                  <td align="right"> 

                    <?php 
                        if(!isset($_SESSION['customer_email'])){
                            echo "<a href=checkout.php>Log in</td>";
                        }else{
                            echo "<a href=logout.php>Log out</td>";
                        }

                    ?>

                    
                </tr>
            </table>

            </div>
            <div class="row carousel-holder">
                <div class="col-md-6">
                    <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
                        <ol class="carousel-indicators">
                            <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
                            <li data-target="#carousel-example-generic" data-slide-to="1"></li>
                            <li data-target="#carousel-example-generic" data-slide-to="2"></li>
                        </ol>
                        <div class="carousel-inner">
                            <div class="item active">
                                <img class="slide-image" src="images/scroll1.jpg" alt="">
                            </div>
                            <div class="item">
                                <img class="slide-image" src="images/scroll2.jpg" alt="">
                            </div>
                            <div class="item">
                                <img class="slide-image" src="images/scroll3.jpg" alt="">
                            </div>
                        </div>
                        <a class="left carousel-control" href="#carousel-example-generic" data-slide="prev">
                            <span class="glyphicon glyphicon-chevron-left"></span>
                        </a>
                        <a class="right carousel-control" href="#carousel-example-generic" data-slide="next">
                            <span class="glyphicon glyphicon-chevron-right"></span>
                        </a>
                    </div>
                </div>
            </div>

           

            <div class = 'row'>
               
                <?php
                    getPro();
                ?>
                <?php
                    getProdType();
                ?>
            </div>
        </div>

        </div>
         <div style="padding:50px">
            <h3 style="font-size:20px">
            <center>
                <img src="images/footer.png" alt="">
            </center>
            </h3>
        </div>
    </div>

    <?php cart(); ?>

      <!-- jQuery -->
    <script src="js/jquery.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="js/bootstrap.min.js"></script>

</body>
</html>